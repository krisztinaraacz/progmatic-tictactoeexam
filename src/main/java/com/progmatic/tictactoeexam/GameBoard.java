/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author progmatic
 */
public class GameBoard implements Board {

    List<Cell> cells = new ArrayList<>();

    public GameBoard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                Cell cell = new Cell(i, j);
                cells.add(cell);
            }
        }
    }

    @Override
    public PlayerType getCell(int rowIdx, int colIdx) throws CellException {
        PlayerType p = null;
        if (rowIdx < 0 || rowIdx > 2) {
            throw new CellException(rowIdx, colIdx, "Non existing row!");
        } else if (colIdx < 0 || colIdx > 2) {
            throw new CellException(rowIdx, colIdx, "Non existing column!");
        } else {
            for (Cell cell : cells) {
                if (cell.getRow() == rowIdx && cell.getCol() == colIdx) {
                    p = cell.getCellsPlayer();
                }
            }
        }
        return p;
    }

    @Override
    public void put(Cell cell) throws CellException {
        if (cell.getRow() < 0 || cell.getRow() > 2) {
            throw new CellException(cell.getRow(), cell.getCol(), "Non existing cell (non existing row)!");
        } else if (cell.getCol() < 0 || cell.getCol() > 2) {
            throw new CellException(cell.getRow(), cell.getCol(), "Non existing cell (non existing column)!");
        } else {
            for (Cell cellOnBoard : cells) {
                if (cellOnBoard.getRow() == cell.getRow() && cellOnBoard.getCol() == cell.getCol()) {
                    if (!cellOnBoard.getCellsPlayer().equals(PlayerType.EMPTY)) {
                        throw new CellException(cell.getRow(), cell.getCol(), "Cell is not empty!");
                    } else {
                        cellOnBoard.setCellsPlayer(cell.getCellsPlayer());
                    }
                }
            }
        }
    }

    @Override
    public boolean hasWon(PlayerType p) {
        List<Cell> cellsOfPlayer = new ArrayList<>();
        for (Cell cell : cells) {
            if (cell.getCellsPlayer().equals(p)) {
                cellsOfPlayer.add(cell);
            }
        }
        if (cellsOfPlayer.size() >= 3) {
            Map<Integer, Integer> rows = new HashMap<>();
            Map<Integer, Integer> columns = new HashMap<>();
            int diagDown = 0;
            int diagUp = 0;
            for (Cell cellOfPlayer : cellsOfPlayer) {
                rows.putIfAbsent(cellOfPlayer.getRow(), 0);
                rows.put(cellOfPlayer.getRow(), rows.get(cellOfPlayer.getRow()) + 1);
                columns.putIfAbsent(cellOfPlayer.getCol(), 0);
                columns.put(cellOfPlayer.getCol(), columns.get(cellOfPlayer.getCol()) + 1);
                if (cellOfPlayer.getRow() == cellOfPlayer.getCol()) {
                    diagDown++;
                }
                if (cellOfPlayer.getRow() + cellOfPlayer.getCol() == 2) {
                    diagUp++;
                }
            }
            return rows.values().contains(3) || columns.values().contains(3) || diagDown == 3 || diagUp == 3;
        } else {
            return false;
        }
    }

    @Override
    public List<Cell> emptyCells() {
        List<Cell> freeCells = new ArrayList<>();
        for (Cell cell : cells) {
            if (cell.getCellsPlayer().equals(PlayerType.EMPTY)) {
                freeCells.add(cell);
            }
        }
        return freeCells;
    }

}
