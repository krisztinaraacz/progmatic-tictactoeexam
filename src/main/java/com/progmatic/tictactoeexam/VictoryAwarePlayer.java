/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.interfaces.AbstractPlayer;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.List;

/**
 *
 * @author progmatic
 */
public class VictoryAwarePlayer extends AbstractPlayer {

    public VictoryAwarePlayer(PlayerType p) {
        super(p);
    }

    @Override
    public Cell nextMove(Board b) {
        PlayerType p = myType;
        Cell cell = null;
        boolean isWinning = false;
        List<Cell> emptyCells = b.emptyCells();
        if (b.emptyCells().isEmpty()) {
            return cell;
        } else {
            for (Cell emptyCell : emptyCells) {
                emptyCell.setCellsPlayer(myType);
                isWinning = b.hasWon(myType);
                emptyCell.setCellsPlayer(PlayerType.EMPTY);
                if (isWinning) {
                    int rowIdx = emptyCell.getRow();
                    int colIdx = emptyCell.getCol();
                    cell = new Cell(rowIdx, colIdx, p);
                    break;
                }
            }
        }
        if (!isWinning) {
            int rowIdx = emptyCells.get(0).getRow();
            int colIdx = emptyCells.get(0).getCol();
            cell = new Cell(rowIdx, colIdx, p);
        }
        return cell;
    }

}
