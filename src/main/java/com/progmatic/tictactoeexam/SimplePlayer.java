/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.interfaces.AbstractPlayer;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.List;

/**
 *
 * @author progmatic
 */
public class SimplePlayer extends AbstractPlayer {

    public SimplePlayer(PlayerType p) {
        super(p);
    }

    @Override
    public Cell nextMove(Board b) {
        Cell cell;
        List<Cell> emptyCells = b.emptyCells();
        if (!emptyCells.isEmpty()) {
            Cell c = emptyCells.get(0);
            int rowIdx = c.getRow();
            int colIdx = c.getCol();
            PlayerType p = myType;
            cell = new Cell (rowIdx, colIdx, p);
        } else {
            return null;
        }
        return cell;
    }

}
